﻿Write-Host 'Caffeinating . . . . . . ' -Foregroundcolor Green
Write-Host " "
Write-Host 'To stop caffeinating, just close the window!'

while (1) {
  $wsh = New-Object -ComObject WScript.Shell
  # sends Shift+F15 keystroke every 60 seconds
  $wsh.SendKeys('+{F15}')
  Start-Sleep -seconds 59
}